resource "aws_db_instance" "default" {
  allocated_storage      = "${var.allocated_storage}"
  storage_type           = "${var.storage_type}"
  engine                 = "${var.engine}"
  engine_version         = "${var.engine_version}"
  instance_class         = "${var.instance_class}"
  name                   = "${var.name}"
  username               = "${var.username}"
  password               = "${var.password}"
  parameter_group_name   = "${var.parameter_group_name}"
  db_subnet_group_name   = "${aws_db_subnet_group.main_db_subnet_group.name}"
  vpc_security_group_ids = ["${var.security_group}"]
  identifier             = "${var.identifier}"
  skip_final_snapshot       = "${var.final}"
}

resource "aws_db_subnet_group" "main_db_subnet_group" {
  name        = "${var.name}-subnetgrp"
  description = "RDS subnet group"
  subnet_ids  = ["${var.subnet}"]
}

### Return database name

output "name" {
  value = "${aws_db_instance.default.name}"
}

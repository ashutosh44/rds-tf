variable "storage_type" {
  description = "type of storage"
}

variable "parameter_group_name" {
  description = "DB parameter group that defines the configuration setting"
}

variable "engine" {
  description = "engine mysql"
}

variable "name" {
  description = "name of database"
}

variable "engine_version" {
  description = "version of engine"
}

variable "instance_class" {
  description = "name of instance"
}

variable "allocated_storage" {
  description = "total allocated storage"
}

variable "username" {
  description = "admin"
}

variable "password" {
  description = "password of database"
}

variable "identifier" {
  description = "cashbook-nonprod"
}

variable "security_group" {
  description = "List of the security group in which RDS instance should be created"
}

variable "subnet" {
  description = "List of the subnets in which RDS instance should be created"
  type        = "list"
}

variable "final" {
  default = "true"
}

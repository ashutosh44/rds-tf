variable "region" {
  description = "Region name"
  default     = "ap-south-1"
}

variable "profile" {
  description = "Aws Profiles for Infra"
  default     = "chqbook"
}

variable "storage_type_" {
  default = "gp2"
}

variable "parameter_group_name_" {
  default = "default.mysql8.0"
}

variable "engine_" {
  default = "mysql"
}

variable "name_" {
  default = "cashbook"
}

variable "engine_version_" {
  default = "8.0"
}

variable "instance_class_" {
  default = "db.t2.small"
}

variable "allocated_storage_" {
  default = "20"
}

variable "username_" {
  default = "admin"
}

variable "password_" {
  default = "chqbook123"
}

variable "identifier_" {
  default = "cashbook-nonprod"
}

variable "subnet_list" {
  type    = "list"
  default = ["subnet-0366e1d3b03fb539d", "subnet-01e05c331ac8b27e3"]
}

variable "security" {
  default = "sg-033cc46de6adb8194"
}

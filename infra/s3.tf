terraform {
  backend "s3" {
    bucket  = "chqbok-devops"
    key     = "cashbook-infra/non-prod/aws-rds.json"
    region  = "ap-south-1"
  }
}
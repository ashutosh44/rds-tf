provider "aws" {
  region = "${var.region}"
  profile = "${var.profile}"
}

module "mysql_rds" {
  source               = "../rds"
  allocated_storage    = "${var.allocated_storage_}"
  storage_type         = "${var.storage_type_}"
  engine               = "${var.engine_}"
  engine_version       = "${var.engine_version_}"
  instance_class       = "${var.instance_class_}"
  name                 = "${var.name_}"
  username             = "${var.username_}"
  password             = "${var.password_}"
  parameter_group_name = "${var.parameter_group_name_}"
  identifier           = "${var.identifier_}"
  subnet               = ["${var.subnet_list}"]
  security_group       = "${var.security}"
  identifier           = "${var.identifier_}"
}
